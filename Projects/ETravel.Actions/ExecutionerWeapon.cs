﻿namespace ETravel.Actions
{
    public enum ExecutionerWeapon
    {
        // Execute nothing, just check parameters across all tasks
        // WARNING: May fail if parameters of one action depend on the result of the previous.
        NerfGun,

        // default behavior: execute until one task fails
        Katana,

        // execute anything and everything you can, despite failures
        Bazooka  
    }
}