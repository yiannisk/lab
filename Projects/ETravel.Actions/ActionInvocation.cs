﻿using System;
using System.Collections.Generic;

namespace ETravel.Actions
{
	public class ActionInvocation
	{
		public Type ActionType { get; set; }
		public Func<bool> Condition { get; set; }
		public Dictionary<string, Func<ActionContext, dynamic>> Parameters { get; private set; }
		
		public ActionInvocation()
		{
			Parameters = new Dictionary<string, Func<ActionContext, dynamic>>();
		}
	}
}