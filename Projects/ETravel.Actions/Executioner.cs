﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ETravel.Actions
{
    public class Executioner
    {
		private ActionContext Context { get; set; }
		private List<ActionInvocation> Sequence { get; set; }
        public ExecutionerWeapon Weapon { get; set; }

	    public Executioner(ActionContext context, ExecutionerWeapon weapon = ExecutionerWeapon.Katana)
	    {
		    Context = context;
	        Weapon = weapon;
			Sequence = new List<ActionInvocation>();
	    }

	    public Executioner Conditional<T>(Func<bool> condition) where T : ActionBase
	    {
		    Sequence.Add(new ActionInvocation
		    {
			    Condition = condition,	
			    ActionType = typeof (T)
		    });

		    return this;
	    }

	    public Executioner Execute<T>() where T : ActionBase
	    {
		    Sequence.Add(new ActionInvocation { ActionType = typeof(T) });
		    return this;
	    }

	    public Executioner Execute(Executioner anotherExecutioner)
	    {
		    Sequence.AddRange(anotherExecutioner.Sequence);
		    return this;
	    }

	    public Executioner WithParameter(string name, Func<ActionContext, dynamic> valueExpression)
	    {
			if (!Sequence.Any())
				throw new InvalidOperationException("No action has been specified yet.");

		    Sequence.Last().Parameters.Add(name, valueExpression);

		    return this;
	    }

	    public AggregateActionResult GetResult()
	    {
		    var results = new List<ActionResult>();

		    foreach (var invocation in Sequence)
		    {
                // TODO: Can collect instance from container here for maximum efficiency.
			    var actionInstance = Activator.CreateInstance(invocation.ActionType) as ActionBase;

				if (actionInstance == null)
					throw new InvalidOperationException(string.Format(
						"Activator failed to instantiate action with type [{0}].", 
						invocation.ActionType.Name));

			    ActionResult result;

			    actionInstance.Parameters = 
					invocation.Parameters.ToDictionary(x => x.Key, x => x.Value(Context));

				if (!actionInstance.VerifyParameters())
				{
					result = new ActionResult(false, exception: new ArgumentException(
						string.Format("Expected parameters for action [{0}] not found.", invocation.ActionType.Name)));

					results.Add(result);

				    if (Weapon != ExecutionerWeapon.Katana) 
                        continue;

					break;
				}

                if (Weapon == ExecutionerWeapon.NerfGun) 
                    continue;

			    result = actionInstance.Execute(Context);
				results.Add(result);

		        if (Weapon == ExecutionerWeapon.Katana && !result.Successful)
		            break;

			    Context.LastResult = result;
		    }

		    return new AggregateActionResult(results);
	    }
    }
}
