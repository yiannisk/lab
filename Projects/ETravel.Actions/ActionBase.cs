﻿using System.Collections.Generic;

namespace ETravel.Actions
{
	public abstract class ActionBase
	{
		public Dictionary<string, dynamic> Parameters { get; set; }

	    public virtual bool VerifyParameters()
	    {
	        return true;
	    }

		public abstract ActionResult Execute(ActionContext context);
	}
}