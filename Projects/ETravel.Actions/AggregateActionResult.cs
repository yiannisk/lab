﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ETravel.Actions
{
	public class AggregateActionResult : IActionResult
	{
		public IList<ActionResult> Results { get; set; }

		public dynamic Data
		{
			get { return Results.Where(x => x.Data != null).Select(x => x.Data).ToList(); }
		}

		public bool Successful
		{
			get { return Results.All(x => x.Successful); }
		}

		public Exception Exception
		{
			get
			{
				return new AggregateException(
                    Results.Where(x => x.Exception != null).Select(x => x.Exception));
			}
		}

		public AggregateActionResult(IList<ActionResult> results)
		{
			Results = results;
		}
	}
}