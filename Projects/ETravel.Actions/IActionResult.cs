﻿using System;

namespace ETravel.Actions
{
	public interface IActionResult
	{
		bool Successful { get; }
		Exception Exception { get; }
		dynamic Data { get; }
	}
}