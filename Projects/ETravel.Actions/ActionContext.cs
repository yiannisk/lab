﻿using System.Collections.Generic;

namespace ETravel.Actions
{
	public class ActionContext : Dictionary<string, dynamic>
	{
		public ActionResult LastResult { get; set; }
	}
}