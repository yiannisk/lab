﻿using System;

namespace ETravel.Actions
{
	public class ActionResult : IActionResult
	{
		public bool Successful { get; private set; }
		public Exception Exception { get; private set; }
		public dynamic Data { get; private set; }

		public ActionResult(bool successful, dynamic data = null, Exception exception = null)
		{
			Successful = successful;
			Exception = exception;
			Data = data;
		}
	}
}