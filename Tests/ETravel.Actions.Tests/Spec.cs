﻿using System.Linq;
using ETravel.Actions.Tests.Samples;
using NUnit.Framework;

namespace ETravel.Actions.Tests
{
	[TestFixture]
    public class Spec
    {
		[Test]
		public void SequenceExecutionDemo()
		{
			var executioner = new Executioner(new ActionContext());

			var result = executioner
				// Conditional execution
				.Conditional<HelloAction>(() => true)
					.WithParameter("name", ctx => "John")
				// Vanilla execution
				.Execute<HelloAction>()
					.WithParameter("name", ctx => "Dolly")
				// Collecting data from context LastResult proxy
				.Execute<HelloAction>()
					.WithParameter("name", ctx => ctx.LastResult.Data)
				.GetResult();

            Assert.IsTrue(result.Successful);
		}

	    [Test]
	    public void SampleFactoryDemo()
	    {
	        var factory = new CommonTasksFactory();
	        var executioner = factory.GetQueryAndParseSomeResult(new ActionContext());

	        Assert.IsFalse(executioner.GetResult().Successful);
	    }

		[Test]
		public void SimpleComposition()
		{
			var factory = new CommonTasksFactory();
			
			var executionerA = factory.GetQueryAndParseSomeResult(new ActionContext());
			var executionerB = factory.GetQueryAndParseSomeResult(new ActionContext());
			
			executionerA.Execute(executionerB);
			
			executionerA.Weapon = ExecutionerWeapon.Bazooka;

			var result = executionerA.GetResult();
			Assert.AreEqual(4, result.Results.Count);
		}


	    [Test]
	    public void NerfShowcase()
        {
            var executioner = new Executioner(new ActionContext())
                // Conditional execution
                .Conditional<HelloAction>(() => true)
                    .WithParameter("name", ctx => "John")
                // Error: task expects "name" parameter but it's not available.
                .Execute<HelloAction>()
                // Vanilla execution
                .Execute<HelloAction>()
                    .WithParameter("name", ctx => "Dick");

            // Using this weapon, we should get a results list containing only errors, if there are any.
            executioner.Weapon = ExecutionerWeapon.NerfGun;
	        
            var result = executioner.GetResult();
	        Assert.AreEqual(1, result.Results.Count);
            Assert.IsFalse(result.Successful);
	    }

	    [Test]
	    public void BazookaShowcase()
	    {
            var executioner = new Executioner(new ActionContext())
                // Conditional execution
                .Conditional<HelloAction>(() => true)
                    .WithParameter("name", ctx => "John")
                // Error: task expects "name" parameter but it's not available.
                .Execute<HelloAction>()
                // Vanilla execution
                .Execute<HelloAction>()
                    .WithParameter("name", ctx => "Dick");

            // Using this weapon, we should get a results list containing all results despite everything.
            executioner.Weapon = ExecutionerWeapon.Bazooka;

            var result = executioner.GetResult();
            Assert.AreEqual(3, result.Results.Count);
            Assert.IsFalse(result.Successful);
            Assert.AreEqual(2, result.Results.Count(x => x.Successful));
            Assert.AreEqual(1, result.Results.Count(x => !x.Successful));
	    }

	    [Test]
	    public void KatanaShowcase()
	    {
            var executioner = new Executioner(new ActionContext())
                // Conditional execution
                .Conditional<HelloAction>(() => true)
                    .WithParameter("name", ctx => "John")
                // Error: task expects "name" parameter but it's not available.
                .Execute<HelloAction>()
                // Vanilla execution
                .Execute<HelloAction>()
                    .WithParameter("name", ctx => "Dick");

            // Using this weapon, we should get a results list containing two results, one successful and one failed,
            // since execution should halt on first head not completely cut off (or action not successful).
            // Note: This is the default weapon executioner comes armed with.
            executioner.Weapon = ExecutionerWeapon.Katana;

            var result = executioner.GetResult();
            Assert.AreEqual(2, result.Results.Count);
            Assert.IsFalse(result.Successful);
            Assert.AreEqual(1, result.Results.Count(x => x.Successful));
            Assert.AreEqual(1, result.Results.Count(x => !x.Successful));
	    }
    }
}
