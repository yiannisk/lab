﻿namespace ETravel.Actions.Tests.Samples
{
    class ParseQueryResultAction : ActionBase
    {
        public override bool VerifyParameters()
        {
            return true;
        }

        public override ActionResult Execute(ActionContext context)
        {
            return new ActionResult(true, "Property 1;Property 2\r\nItem 1;Item 2");
        }
    }
}