using System;
using System.Linq;

namespace ETravel.Actions.Tests.Samples
{
    class SomeQueryAction : ActionBase
    {
        public override bool VerifyParameters()
        {
            return Parameters.ContainsKey("csvText")
                   && Parameters["csvText"] is string;
        }

        public override ActionResult Execute(ActionContext context)
        {
            var csvText = Parameters["csvText"] as string ?? string.Empty;
            
            var skip = Parameters.ContainsKey("headers") && bool.Parse(Parameters["headers"]) ? 1 : 0;
            var lines = csvText.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);

            // We want this action to fail if any rows are returned.
            return new ActionResult(lines.Skip(skip).Any(), csvText);
        }
    }
}