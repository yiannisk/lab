﻿namespace ETravel.Actions.Tests.Samples
{
    class CommonTasksFactory
    {
        public Executioner GetQueryAndParseSomeResult(ActionContext context)
        {
            var executioner = new Executioner(context);

            return executioner
                .Execute<SomeQueryAction>()
                // Here we can use the last action's result as part of the 
                // glue code. We could just as easily assign it with a key to the 
                // context itself.
                .Execute<ParseQueryResultAction>()
                    .WithParameter("csvText", ctx => ctx.LastResult)
                    .WithParameter("headers", ctx => true);
        }
    }
}