using System;

namespace ETravel.Actions.Tests.Samples
{
    class HelloAction : ActionBase
    {
        public override bool VerifyParameters()
        {
            return Parameters.ContainsKey("name")
                && Parameters["name"] is string;
        }

        public override ActionResult Execute(ActionContext context)
        {
            try
            {
                Console.WriteLine("Hello {0}!", Parameters["name"] as string);
                return new ActionResult(true, Parameters["name"]);
            }
            catch (Exception exception)
            {
                return new ActionResult(false, exception: exception);
            }
        }
    }
}